setlocal autoindent
setlocal ts=2
setlocal shiftwidth=2

function! CssClean()
    %s@\v/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/@@g
    %le
    %s/{\_.\{-}}/\=substitute(submatch(0), '\n', '', 'g')/
    nohl
    normal! Gdd
endfunction
