" File ~/.vim/ftplugin/all.vim

source ~/.vim/ftplugin/all.vim

setlocal cindent
setlocal smartindent
setlocal autoindent
setlocal expandtab
setlocal ts=4
setlocal shiftwidth=4
