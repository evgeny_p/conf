;; init repos
(require 'package)
(add-to-list 'package-archives
    '("melpa" . "http://melpa.milkbox.net/packages/") t)

;; setup load paths, etc
(package-initialize)

(setq load-dirs '("~/.emacs.d/0x10/common"
                  "~/.emacs.d/0x10/current"))
