(add-hook 'c-mode-common-hook
          (lambda ()
            (setq c-basic-offset 4
                  tab-width 4)

            (c-add-style "work-style"
                         '((c-offsets-alist
                             (substatement-open . 0)
                             (inline-open . 0)
                             (member-init-intro . 0)
                             (arglist-cont-nonempty . +)
                             (innamespace . 0))))

            (c-set-style "work-style")))

