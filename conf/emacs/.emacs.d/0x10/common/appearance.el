(tool-bar-mode -1)
(scroll-bar-mode -1)
(setq inhibit-startup-message t)
(global-hl-line-mode 1)
(fset 'yes-or-no-p 'y-or-n-p)

(require 'color-theme)
(color-theme-solarized-light)

(require 'centered-cursor-mode)
(global-centered-cursor-mode 1)

(ido-mode)
(show-paren-mode)
