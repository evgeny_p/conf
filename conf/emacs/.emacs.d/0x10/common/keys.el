(define-key global-map (kbd "RET") 'newline-and-indent)

(global-set-key (kbd "<C-prior>") 'buf-move-up)
(global-set-key (kbd "<C-next>") 'buf-move-down)
(global-set-key (kbd "<C-S-home>") 'buf-move-left)
(global-set-key (kbd "<C-S-end>") 'buf-move-right)

