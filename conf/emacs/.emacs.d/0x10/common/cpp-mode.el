(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))

(add-hook 'c-mode-common-hook
          (lambda ()
            (linum-mode 1)
            ;(company-mode)
            ;(irony-mode)
            ;(eval-after-load 'company
            ;                 '(add-to-list 'company-backends 'company-irony))
            ;; (optional) adds CC special commands to `company-begin-commands' in order to
            ;; trigger completion at interesting places, such as after scope operator
            ;;     std::|
            ;(add-hook 'irony-mode-hook
            ;          'company-irony-setup-begin-commands)

            ;(add-hook 'before-save-hook 'delete-trailing-whitespace)

            (ac-config-default)
            (add-to-list 'ac-sources 'ac-source-semantic)
            ;;(require 'semantic/ia)
            (semantic-mode)
            (semantic-idle-local-symbol-highlight-mode)
            ))

