#!/bin/bash

main() {
    cd conf

    for package in *; do
        stow -t $HOME $package
    done

    cd ..
}

main
